#!/usr/bin/python
# -*- coding: UTF-8 -*-
# @Author: Miroslav Hudec
# @Date:   2017-10-10T15:01:11+02:00
# @Email:  mijujda@gmail.com
# @Last modified by:   Miroslav Hudec
# @Last modified time: 2017-10-16T20:57:24+02:00

# Random Graphs


from common_functions import PltClear, Histogram
import networkx as nx
import matplotlib.pyplot as plt

def regular_graph():
    Graph = nx.random_regular_graph(2,100)
    return Graph

def erdos_renyi():
    Graph = nx.erdos_renyi_graph(1000, 0.1)
    return Graph

def watts_strogatz():
    Graph = nx.watts_strogatz_graph(1000,3,0.4)
    return Graph


def barabasi_albert_graph():
    Graph = nx.barabasi_albert_graph(1000,3)
    return Graph

def main():
    #graph = regular_graph()
    #graph = erdos_renyi()
    #graph = watts_strogatz()
    graph = barabasi_albert_graph()

    nx.draw_circular(graph, node_size=20)
    plt.draw()
    plt.show()
    PltClear()

    degrees = nx.degree(graph)
    print(degrees)

    degrees = [x[1] for x in nx.degree(graph)]
    print(degrees)
    Histogram(111, x=degrees)


if __name__ == '__main__':
    main()
