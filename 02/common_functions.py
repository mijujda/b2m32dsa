#!/usr/bin/python
# -*- coding: UTF-8 -*-
# @Author: Miroslav Hudec
# @Date:   2017-10-10T14:57:51+02:00
# @Email:  mijujda@gmail.com
# @Last modified by:   Miroslav Hudec
# @Last modified time: 2017-10-10T17:13:54+02:00



import os
import matplotlib.pyplot as plt
import networkx as nx

def SetOutPN(folder_name):
    work_directory = os.path.dirname(os.path.abspath(__file__))
    print("Current working directory is: '{0}'".format(work_directory))
    if not os.path.exists("{0}/{1}".format(work_directory, folder_name)):
        os.makedirs("{0}/{1}".format(work_directory, folder_name))
    else:
        print("Output folder already exists.")

def Histogram(sPlotIndex, x=None, xLabel='', yLabel='', num_bins=100, log=False,
    normed=0, colorLabel=None, alpha=None, **kwargs):

    fig = plt.figure()
    fig.set_size_inches(4, 3) # width and height in inches
    ax = plt.subplot(sPlotIndex)
    n, bins, patches = plt.hist(x, num_bins, normed=normed, log = log, facecolor='green',
    alpha=0.5)
    if log:
        plt.xscale('log')
        plt.ylim(ymin=0.5)
    if xLabel: plt.xlabel(xLabel)
    if yLabel: plt.ylabel(yLabel)
    plt.draw()
    plt.show()
    PltClear()

def PltClear():
    plt.clf()
    fig = plt.gcf()
    plt.close(fig)
    plt.close('all')
