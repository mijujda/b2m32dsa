# @Author: Miroslav Hudec
# @Date:   2017-10-24T13:05:40+02:00
# @Email:  mijujda@gmail.com
# @Last modified by:   Miroslav Hudec
# @Last modified time: 2017-10-31T17:27:47+01:00
import dpkt
import socket
import datetime
import json
import dns.resolver
import os
import networkx as nx
import matplotlib.pyplot as plt

class DNSResolver:
    def __init__(self):
        self.hostname_map = {}
        self.hostname_map_fin = {}
        self.ip_hostname = {}           # Dictionary holding list of hostnames for IP addr: {"10.1.10.1": [hostname1, hostname2]}
        self.hostname_ip = {}           # Dictionary inverse to ip_hostname: {"hostname": [10.1.10.1, 10.1.10.2, 10.1.10.37]}
        self.cname_hostname = {}

    def parse_frame(self, dns_data):
        if len(dns_data.an) > 0:
            cnames = {}
            hostnames = {}
            for answer in dns_data.an:

                if answer.type == dpkt.dns.DNS_A:                   # If answer contains IP address
                    address = socket.inet_ntoa(answer.rdata)        # Convert address from hex to decimal
                    hostname = answer.name
                    hostnames[hostname] = address                   # Hostname
                    #print("Hostname: {0} - IP: {1}".format(hostname, address))
                    if address not in self.ip_hostname.keys():      # If IP adress is not key of self.ip_hostname, add it
                        self.ip_hostname[address] = []
                    if hostname not in self.ip_hostname[address]:   # If hostname is not in the list yet
                        self.ip_hostname[address].append(hostname)  # Append the hostname to the list of hostnames for specific IP
                    if hostname not in self.hostname_ip.keys():     # If hostname is not key of self.hostname_ip, add it
                        self.hostname_ip[hostname] = []
                    if address not in self.hostname_ip[hostname]:   # If IP address is not in list yet
                        self.hostname_ip[hostname].append(address)  # Append the IP address to the list of IPs for specific hostname

                elif answer.type == dpkt.dns.DNS_CNAME:             # If answer returns hostname for CNAME
                    cname = answer.cname
                    hostname = answer.name
                    cnames[hostname] = cname
                    if cname not in self.cname_hostname.keys():
                        self.cname_hostname[hostname] = []
                    if hostname not in self.cname_hostname[hostname]:
                        self.cname_hostname[hostname].append(cname)
                    #print("Hostname: {0} - CNAME: {1}".format(hostname, cname))
                else:
                    pass
            #print("CNAMEs", cnames)
            #print("HOSTNAMES", hostnames)
            for cname, hostname in cnames.items():
                if hostname in hostnames.keys():
                    address = hostnames[hostname]
                    if cname in self.hostname_ip.keys() and address not in self.hostname_ip[cname]:
                        self.hostname_ip[cname].append(address)
                    elif cname not in self.hostname_ip.keys():
                        self.hostname_ip[cname] = [address]

                    if address in self.ip_hostname.keys() and cname not in self.ip_hostname[address]:
                        self.ip_hostname[address].append(cname)
                    elif address not in self.ip_hostname.keys():
                        self.ip_hostname[address] = [cname]

    def hostname_filter(self, pattern):
        addresses = set()
        for hostname, ips in self.hostname_ip.items():
            if pattern in hostname:
                for ip in ips:
                    addresses.add(ip)
        addresses = list(addresses)
        return addresses

class Packet:
    def __init__(self, timestamp, raw_frame, dns_resolver):
        self.unix_timestamp = timestamp
        self.raw_frame = dpkt.ethernet.Ethernet(raw_frame)
        self.data = None
        self.type = None
        self.src_mac = None
        self.dst_mac = None
        self.timestamp = None
        self.src_ip = None
        self.dst_ip = None
        self.ip_length = None
        self.protocol = None
        self.src_port = None
        self.dst_port = None
        self.dns_resolver = dns_resolver


        self.load()

    def mac_addr(self, address):
        return ":".join("%02x" % dpkt.compat.compat_ord(x) for x in address)

    def ip_addr(self, address):
        # First try IPv4, then IPv6
        try:
            return socket.inet_ntop(socket.AF_INET, address)
        except:
            return socket.inet_ntop(socket.AF_INET6, address)

    def ip_packet(self):
        # IP Packet Section
        packet = self.raw_frame.data
        self.src_ip = self.ip_addr(packet.src)
        self.dst_ip = self.ip_addr(packet.dst)
        self.ip_length = packet.len
        self.ttl = packet.ttl
        self.data.update({"ip": {
            "src_ip": self.src_ip,
            "dst_ip": self.dst_ip,
            "length": self.ip_length,
            "ttl": self.ttl
            }
        })
        # Transport section
        if packet.p == dpkt.ip.IP_PROTO_TCP:
            segment = packet.data
            self.src_port = segment.sport
            self.dst_port = segment.dport
            self.protocol = "tcp"
            self.data["ip"].update({"tcp": {
                "src_port": self.src_port,
                "dst_port": self.dst_port
            }})
        elif packet.p == dpkt.ip.IP_PROTO_UDP:
            segment = packet.data
            self.src_port = segment.sport
            self.dst_port = segment.dport
            self.protocol = "udp"
            self.data["ip"].update({"udp": {
                "src_port": self.src_port,
                "dst_port": self.dst_port
            }})
            if self.src_port == 53:
                try:
                    dns = dpkt.dns.DNS(segment.data)
                    self.dns_resolver.parse_frame(dns)
                except Exception as e:
                    #print("Exception: {0}".format(e))
                    pass
        else:
            #print("Unknown transport protocol.")
            pass

    def arp_packet(self):
        packet = self.raw_frame.data
        if packet.op == dpkt.arp.ARP_OP_REQUEST:
            # We don't care about requests
            print(f"ARP Request SRC-MAC {self.mac_addr(packet.sha)} DST-MAC {self.mac_addr(packet.tha)}")
        elif packet.op == dpkt.arp.ARP_OP_REPLY:
            self.src_mac = self.mac_addr(packet.sha)
            self.dst_mac = self.mac_addr(packet.tha)
            print("ARP Reply")

    def load(self):

        # Ethernet Frame Section
        self.src_mac = self.mac_addr(self.raw_frame.src)
        self.dst_mac = self.mac_addr(self.raw_frame.dst)
        self.timestamp = str(datetime.datetime.utcfromtimestamp(self.unix_timestamp))
        self.data = {
            "timestamp": self.timestamp,
            "src_mac": self.src_mac,
            "dst_mac": self.dst_mac
        }
        if self.raw_frame.type == dpkt.ethernet.ETH_TYPE_IP:
            self.type = "IP"
            self.ip_packet()
        elif self.raw_frame.type == dpkt.ethernet.ETH_TYPE_IP6:
            self.type = "IPv6"
            # We dont care about IPv6
            #self.ip_packet()
            pass

        elif self.raw_frame.type == dpkt.ethernet.ETH_TYPE_ARP:
            self.type = "ARP"
            # ARP might be added later ;)
            #self.arp_packet()

        else:
            #print(f"Got Frame with EtherType {self.raw_frame.type}")
            pass




    def __str__(self):
        return "[SrcMAC {0} SrcIP {1} SrcPORT {2} > DstMAC {3} DstIP {4} DstPort {5}]".format(
            self.src_mac,
            self.src_ip,
            self.src_port,
            self.dst_mac,
            self.dst_ip,
            self.dst_port
        )

    def __repr__(self):
        return "[SrcMAC {0} SrcIP {1} SrcPORT {2} > DstMAC {3} DstIP {4} DstPort {5}]".format(
            self.src_mac,
            self.src_ip,
            self.src_port,
            self.dst_mac,
            self.dst_ip,
            self.dst_port
        )



class PcapParser:
    def __init__(self, filepath):
        #print("Creating Instance of PcapParser...")
        self.filepath = filepath
        self.pcap = self.load_file()
        self.packet_count = 0
        self.packets = []
        self.mac = set()
        self.ip = set()
        self.srcip = set()
        self.dstip = set()
        self.tcp_count = 0
        self.udp_count = 0
        self.arp = {}
        self.dns_resolver = DNSResolver()
        self.load_raw_frames()
        self.mac_ip()
        self.viber_addresses = self.dns_resolver.hostname_filter("viber")
        self.viber_hostname_map = {}
        self.viber_hostname_mapper()

    def draw_arp (self):
        g = nx.Graph()
        for k,v in self.arp.items():
            for ip in v:
                g.add_edge(k, ip)
        nx.draw_spring(g, with_labels=True, node_size=10)
        plt.draw()
        plt.show()

    def mac_ip (self):
        for packet in self.packets:
            if packet.src_mac not in self.arp.keys():
                self.arp[packet.src_mac] = set()
            self.arp[packet.src_mac].add(packet.src_ip)
            if packet.dst_mac not in self.arp.keys():
                self.arp[packet.dst_mac] = set()
            self.arp[packet.dst_mac].add(packet.dst_ip)

    def draw_ip(self, filename=None):

        graph = nx.Graph()
        color_map = []
        for packet in self.packets:
            graph.add_edge(packet.src_ip, packet.dst_ip)
        for node in graph:
            if node in self.viber_addresses:
                color_map.append("blue")
            else:
                color_map.append("red")
        pos = {node: (pos[0], pos[1]) for (node, pos) in nx.spring_layout(graph).items()}
        labels = {}
        for node in graph:
            if node in self.viber_addresses:
                for hostname in self.dns_resolver.ip_hostname[node]:
                    if "viber" in hostname:
                        labels[node] = hostname
                        break
            else:
                labels[node] = node
        nx.draw(graph, pos=pos, node_color=color_map, node_size=10, with_labels=False)
        nx.draw_networkx_labels(graph, pos, labels=labels, font_size=7)
        plt.draw()
        if filename:
            plt.savefig(filename)
        else:
            plt.show()


    def load_file(self):
        pcap_file = dpkt.pcap.Reader(open(self.filepath, mode='rb'))
        return pcap_file

    def load_raw_frames(self, layer=2):
        print("Loading RAW frames...")
        i = 1
        for raw_frame in self.pcap:
            #print(f"Processing frame {i}", end="\r")
            packet = Packet(*raw_frame, self.dns_resolver)
            if packet.type == "IP":
                self.packets.append(packet)
            i += 1
        #print("")

    def counter (self):
        for packet in self.packets:
            self.ip.add(packet.src_ip)
            self.srcip.add(packet.src_ip)
            self.ip.add(packet.dst_ip)
            self.dstip.add(packet.dst_ip)
            self.mac.add(packet.src_mac)
            self.mac.add(packet.dst_mac)
            self.packet_count+=1
            if packet.protocol == "tcp":
                self.tcp_count += 1
            if packet.protocol == "udp":
                self.udp_count += 1

    def viber_hostname_mapper(self):
        for ip in self.viber_addresses:
            for hostname in self.dns_resolver.ip_hostname[ip]:
                if "viber" in hostname:
                    if hostname not in self.viber_hostname_map.keys():
                        self.viber_hostname_map[hostname] = []
                    self.viber_hostname_map[hostname].append(ip)

    def __str__(self):
        return "[PcapParserObject: {0} Packets]".format(len(self.packets))




def main():
    pcap_parser = PcapParser("{0}\\20141021merged.pcap".format(os.path.dirname(os.path.abspath(__file__))))

    pcap_parser.counter()
    pcap_parser.draw_arp()
    print(f"Number of unique IP addresses: {len(pcap_parser.ip)}")
    print(f"Number of IPv4 packets: {pcap_parser.packet_count}")
    arp = {mac: list(ips) for (mac, ips) in pcap_parser.arp.items()}
    #print(json.dumps(arp, indent=2))
    # IP addresses for hostname containing viber
    viber_addresses = pcap_parser.dns_resolver.hostname_filter("viber")
    print(f"Number of Viber addresses: {len(viber_addresses)}")
    pcap_parser.draw_ip()#filename=f"{os.path.dirname(__file__)}/figures/ip_network.png")
    print("Viber Hostname Map")

    for hostname, ips in pcap_parser.viber_hostname_map.items():
        print(hostname, " - ", *ips)







if __name__ == '__main__':
    main()
