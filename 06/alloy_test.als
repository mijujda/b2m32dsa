module Conference
abstract sig Person {}
one sig A, B, C, D, E, F extends Person {}
sig State {go, dont_go: set Person}


all p, q in (A + D) | p in Comes => q not in Comes