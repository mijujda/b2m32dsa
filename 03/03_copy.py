#!/usr/bin/python
# -*- coding: UTF-8 -*-
# @Author: Miroslav Hudec
# @Date:   2017-10-17T14:35:36+02:00
# @Email:  mijujda@gmail.com
# @Last modified by:   Miroslav Hudec
# @Last modified time: 2017-10-31T15:25:36+01:00

import os
import re
import json
import networkx as nx
import matplotlib.pyplot as plt
from io import TextIOWrapper as TextCodecWrapper
import gzip
import csv
import numpy as np
import operator

class FlowParser:
    def __init__(self, folder_path=None, file_count=None, protocols=None, max_ports=None):
        # Variables
        self.folder_path = folder_path
        self.file_count = file_count
        self.file_paths = []
        self.data = []
        self.protocols = protocols
        self.max_ports = max_ports
        self.headers = ["Src_IP", "Dst_IP", "Protocol", "Src_Port", "Dst_Port", "Octects", "Packets"]
        self.src_ips, self.dst_ips, self.ips = (set(), set(), set())
        self.packet_count = None
        self.octects_count = None
        self.neighbors = {}
        self.netflow_count = None
        self.packet_count = None
        self.octects_count = None
        self.system_ports = {}

        # Run Functions
        self.get_file_paths()   # Populate self.file_paths
        self.load_data(protocols=self.protocols)        # Populate self.data based on self.file_paths
        self.generate_neighbors()



    def get_file_paths(self):
        """
        This function finds all the files in given folder
        and returns a list of full paths to these files
        """

        cwd = os.path.dirname(os.path.abspath(__file__))
        if not os.path.exists(self.folder_path):
            self.folder_path = "{0}\{1}".format(cwd, self.folder_path)
            if not os.path.exists(self.folder_path):
                print("No such folder: {0}".format(self.folder_path))
        files = os.listdir(self.folder_path)
        for f in files:
            if not os.path.isfile("{0}\{1}".format(self.folder_path, f)):
                files.remove(f)
        paths = ["{0}\{1}".format(self.folder_path, x) for x in files]
        if self.file_count:
            self.file_paths = paths[:self.file_count]
        else:
            self.file_paths = paths

    def load_data(self, protocols=None, max_ports=None):
        """
        This function handles reading data from zipped CSV files.
        It is posible to filter the data input by providing protocol number list; ex.: [6, 17] for TCP and UDP;
        or max_ports number; ex.: 1024 for flows that contain well known ports.
        This function is called by the class __init__ function,
        which provides protocols=self.protocols and max_ports=self.max_ports.
        Function appends loaded entries to self.data - nested list.
        Some counters are increased during the data loading.
        """
        self.packet_count = 0
        self.octects_count = 0
        for filename in self.file_paths:
            with open(filename, mode='rb') as inBinF:
                inGZF = TextCodecWrapper(gzip.GzipFile(fileobj = inBinF, mode = 'r'), "utf-8", errors='ignore')

                dataReader = csv.reader(inGZF, delimiter=',', quotechar='"')
                for line in dataReader:
                    line = line[0:2] + [int(x) for x in line[2:]]
                    if protocols:
                        if line[2] in protocols:    # If protocol matches
                            self.packet_count += line[6]
                            self.octects_count += line[5]
                            if max_ports:
                                if line[3] <= max_ports or line[4] <= max_ports:
                                    self.data.append(line)
                            else:
                                self.data.append(line)

                    else:
                        self.packet_count += line[6]
                        self.octects_count += line[5]
                        if max_ports:
                            if line[3] <= max_ports or line[4] <= max_ports:
                                self.data.append(line)
                        else:
                            self.data.append(line)

                    # Increase IPs counters
                    self.src_ips.add(line[0])
                    self.dst_ips.add(line[1])
                    self.ips.add(line[0])
                    self.ips.add(line[1])

                    # Add System ports
                    if line[4] <= 1024:
                        if line[4] not in self.system_ports.keys():
                            self.system_ports[line[4]] = {"dst_ips": set(), "flow_count": 1}
                            self.system_ports[line[4]]["dst_ips"].add(line[1])
                        else:
                            self.system_ports[line[4]]["flow_count"] += 1
                            self.system_ports[line[4]]["dst_ips"].add(line[1])

        # Total number of imported flows
        self.netflow_count = len(self.data)

    def system_port_filter(self, flow_count, dst_ips_count):
        """
        This function returns set() of system ports, which match given criteria
        """
        filtered_ports = set()
        for port, data in self.system_ports.items():
            if len(data["dst_ips"]) >= dst_ips_count and data["flow_count"] >= flow_count:
                filtered_ports.add(port)
        return list(sorted(filtered_ports, reverse=False))

    def protocols_distribution(self, by_property=None, percentage=False):
        """
        This function calculates distribution of protocols in self.data.
        By default, distribution is calculated based number of flows,
        but can be calculated based on "Packets" or "Octects".

        """
        protocols = {}
        self.packet_count = 0
        self.octects_count = 0
        if by_property:
            row = self.headers.index(by_property)
            for flow in self.data:
                self.packet_count += flow[6]
                self.octects_count += flow[5]
                if flow[2] in protocols.keys():
                    protocols[flow[2]] += flow[row]
                else:
                    protocols[flow[2]] = flow[row]
            if percentage and by_property == "Packets":
                for protocol in protocols.keys():
                    protocols[protocol] = (protocols[protocol] / self.packet_count) * 100
            if percentage and by_property == "Octects":
                for protocol in protocols.keys():
                    protocols[protocol] = (protocols[protocol] / self.octects_count) * 100
        else:
            # By Flow Count
            for flow in self.data:
                self.packet_count += flow[6]
                self.octects_count += flow[5]
                if flow[2] in protocols.keys():
                    protocols[flow[2]] += 1
                else:
                    protocols[flow[2]] = 1
            if percentage:
                for protocol in protocols.keys():
                    protocols[protocol] = (protocols[protocol] / self.netflow_count) * 100
        return protocols

    def port_distribution(self, by_property):
        ports = {}
        # By flow count
        if by_property:
            row = self.headers.index(by_property)
            for flow in self.data:
                # Source port
                if flow[3] not in ports.keys():
                    ports[flow[3]] = flow[row]
                else:
                    ports[flow[3]] += flow[row]
                # Destination port
                if flow[4] not in ports.keys():
                    ports[flow[4]] = flow[row]
                else:
                    ports[flow[4]] += flow[row]
        else:
            for flow in self.data:
                # Source port
                if flow[3] not in ports.keys():
                    ports[flow[3]] = 1
                else:
                    ports[flow[3]] += 1
                # Destination port
                if flow[4] not in ports.keys():
                    ports[flow[4]] = 1
                else:
                    ports[flow[4]] += 1
        return ports

    def generate_neighbors(self):
        for flow in self.data:
            if flow[0] not in self.neighbors.keys():
                self.neighbors[flow[0]] = {}
            if flow[1] not in self.neighbors[flow[0]].keys():
                self.neighbors[flow[0]].update({flow[1]: {"protocols": set(), "packets": flow[6], "octects": flow[5], "ports": set()}})
                self.neighbors[flow[0]][flow[1]]["protocols"].add(flow[2])
                self.neighbors[flow[0]][flow[1]]["ports"].add(flow[3])
                self.neighbors[flow[0]][flow[1]]["ports"].add(flow[4])
            else:
                self.neighbors[flow[0]][flow[1]]["protocols"].add(flow[2])
                self.neighbors[flow[0]][flow[1]]["ports"].add(flow[3])
                self.neighbors[flow[0]][flow[1]]["ports"].add(flow[4])
                self.neighbors[flow[0]][flow[1]]["packets"] += flow[6]
                self.neighbors[flow[0]][flow[1]]["octects"] += flow[5]

    def top_ten(self, data, top=10):
        top_dict = dict(sorted(data.items(), key=operator.itemgetter(1), reverse=True)[:top])
        return top_dict

    def draw_network(self, protocol=None, port=None, labels=False, filename=None):
        graph = nx.Graph()
        fig = plt.figure()
        ax = plt.subplot(111)
        for node, neighbors in self.neighbors.items():
            for neighbor in neighbors.keys():
                if protocol and port:
                    if protocol in neighbors[neighbor]["protocols"] and port in neighbors[neighbor]["ports"]:
                        graph.add_edge(node, neighbor)
                elif protocol and not port:
                    if protocol in neighbors[neighbor]["protocols"]:
                        graph.add_edge(node, neighbor)
                elif not protocol and port:
                    if port in neighbors[neighbor]["ports"]:
                        graph.add_edge(node, neighbor)
                else:
                    graph.add_edge(node, neighbor)
        nx.draw_circular(graph, node_size=10, with_labels=labels)
        plt.draw()
        if filename:
            plt.savefig("{0}\\figures\\{1}".format(os.path.dirname(os.path.abspath(__file__)), filename))
        else:
            plt.show()
        self.PltClear()
        return graph

    def draw_degree_distribution(self, graph, filename=None):
        degree = nx.degree(graph)
        #print(degree)
        degree_sequence = list(sorted([x[1] for x in degree], reverse=True))
        #print("Degree sequence:", degree_sequence)
        fig = plt.figure()
        ax = plt.subplot(111)
        plt.loglog(degree_sequence,'r-',marker='o')
        plt.title("Degree rank plot")
        plt.ylabel("Degree")
        plt.xlabel("Rank")

        if filename:
            plt.savefig("{0}\\figures\\{1}".format(os.path.dirname(os.path.abspath(__file__)), filename))
        else:
            plt.show()
        self.PltClear()
        return degree

    def draw_histogram3(self, x, y1, y2, y3, label1, label2, label3, xlabel, ylabel, filename=None):
        groups = len(x)
        index = np.arange(groups)
        bar_width = 0.2
        opacity = 0.5
        fig, ax = plt.subplots()
        plt.bar(index, y1, bar_width, alpha=opacity, color="r", label=label1)
        plt.bar(index + bar_width, y2, bar_width, alpha=opacity, color="g", label=label2)
        plt.bar(index + bar_width * 2, y3, bar_width, alpha=opacity, color="b", label=label3)

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.xticks(index + 3*bar_width / 3, x)
        axes = plt.gca()
        axes.set_ylim([0, 100])
        plt.tight_layout()
        plt.legend()
        if filename:
            plt.savefig("{0}\\figures\\{1}".format(os.path.dirname(os.path.abspath(__file__)), filename))
        else:
            plt.show()
        self.PltClear()

    def draw_rich_club_coef(self, graph, label, filename=None):
        rc = nx.rich_club_coefficient(graph, normalized=False)
        rc_sorted = list(sorted(rc.items(), key=operator.itemgetter(0)))
        degrees_sorted = [x[0] for x in rc_sorted]
        rc_coef_sorted = [x[1] for x in rc_sorted]
        #print(rc_sorted)
        self.draw_histogram(degrees_sorted, rc_coef_sorted, label, "Degree", "Rich Club Coefficient", filename=filename)

        return degrees_sorted[rc_coef_sorted.index(max(rc_coef_sorted))]

    def draw_rc_network(self, graph, degree, max_rc_degree, filename=None):
        degree = dict(degree)
        new_graph = nx.Graph()
        for edge in graph.edges():
            if degree[edge[0]] == max_rc_degree or degree[edge[1]] == max_rc_degree:
                new_graph.add_edge(*edge)
        fig = plt.figure()
        ax = plt.subplot(111)
        nx.draw_spring(new_graph, node_size=10, with_labels=True)
        plt.draw()
        if filename:
            plt.savefig("{0}\\figures\\{1}".format(os.path.dirname(os.path.abspath(__file__)), filename))
        else:
            plt.show()
        self.PltClear()
        return graph

    def draw_histogram(self, x, y, label, xlabel, ylabel, filename=None, color="r"):
        groups = len(x)
        index = np.arange(groups)
        bar_width = 0.5
        opacity = 1
        fig, ax = plt.subplots()
        plt.bar(index, y, bar_width, alpha=opacity, color=color, label=label)

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.xticks(index, x)
        plt.tight_layout()
        plt.legend()
        if filename:
            plt.savefig("{0}\\figures\\{1}".format(os.path.dirname(os.path.abspath(__file__)), filename))
        else:
            plt.show()
        self.PltClear()

    def draw_huge_histogram(self, x, num_bins, label, xlabel, ylabel, color, filename=None):
        fig, ax = plt.subplots()
        plt.hist(x, bins=num_bins, facecolor=color, label=label, range=[0, 65535])
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.legend()
        plt.tight_layout()
        if filename:
            plt.savefig("{0}\\figures\\{1}".format(os.path.dirname(os.path.abspath(__file__)), filename))
        else:
            plt.show()
        self.PltClear()


    def sorter(self, data):
        sor = list(sorted(data.items(), key=operator.itemgetter(0)))
        x, y = ([x[0] for x in sor], [x[1] for x in sor])
        #x = list(data.keys())
        #x.sort()
        #y = [data[key] for key in x]

        return x, y

    def PltClear(self):
        plt.clf()
        fig = plt.gcf()
        plt.close(fig)
        plt.close('all')

    def __str__(self):
        return "FlowParserObject:_{0}_Flows".format(self.netflow_count)

def main():
    # TCP = 6
    # UDP = 17
    flow_parser = FlowParser(folder_path="zip", file_count=None, protocols=[17], max_ports=1024)
    print("NetFlows Count: {0}".format(flow_parser.netflow_count))
    print("Packets Count: {0}".format(flow_parser.packet_count))
    print("Octects Count: {0}".format(flow_parser.octects_count))
    print("Source IPs Count: {0}".format(len(flow_parser.src_ips)))
    print("Destination IPs Count: {0}".format(len(flow_parser.dst_ips)))
    print("Total IPs Count: {0}".format(len(flow_parser.ips)))

    flow_parser.draw_network(protocol=17, port=53, labels=True)

if __name__ == '__main__':
    main()
