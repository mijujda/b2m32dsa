
import networkx as nx
import csv
import os
import matplotlib.pyplot as plt

dir_path = os.path.dirname(os.path.realpath(__file__))

measurements_file = "measurement.csv"
measurements_file_path = "{0}/{1}".format(dir_path, measurements_file)

data = None

with open(measurements_file_path, mode="r") as file:
    data = [x.split(";") for x in file.readlines()]

headers = data[0]
data = data[1:]
# Clear end of line
headers[-1] = headers[-1].strip()
for line in data:
    line[-1] = line[-1].strip()
    line = line[:-1]


print(headers)
for line in data:
    print(line[2], line[7])

g = nx.Graph()
for line in data:
    g.add_edge(line[2], line[7])

#nx.draw(g)
#plt.draw()

