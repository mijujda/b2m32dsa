#!/usr/bin/python
# -*- coding: UTF-8 -*-
# @Author: Miroslav Hudec
# @Date:   2017-10-09T10:36:03+02:00
# @Email:  mijujda@gmail.com
# @Last modified by:   Miroslav Hudec
# @Last modified time: 2017-10-10T15:54:36+02:00


import networkx as nx
from networkx.algorithms import bipartite
import csv
import os
import matplotlib.pyplot as plt
import plotly.plotly as py
import json


def load_data(filename):
    lines = []
    data = {}

    with open(filename, mode='r') as f:
        lines = f.read().split("\n")
    lines = [x.split(";") for x in lines[:-1]]
    for line in lines[1:]:
        # Date filter
        #if "10/05" in line[7]:
            #continue
        if line[7] not in data.keys():
            data[line[7]] = {
                line[2]:
                    {
                    "id": line[0],
                    "mgid": line[1],
                    "bssid": line[2],
                    "ssid": line[3],
                    "rssi": int(line[4]),
                    "channel": line[5],
                    "security": line[6],
                    "timestamp": line[7],
                    "cssid": line[8],
                    "vcc": line[9],
                    }
                }
        else:
            data[line[7]].update({
                line[2]:
                    {
                    "id": line[0],
                    "mgid": line[1],
                    "bssid": line[2],
                    "ssid": line[3],
                    "rssi": int(line[4]),
                    "channel": line[5],
                    "security": line[6],
                    "timestamp": line[7],
                    "cssid": line[8],
                    "vcc": line[9],
                    }
            })
    return data

def generate_position(nodes, start, step):
    node_pos = {}
    for node in nodes:
        node_pos[node] = (start[0], start[1])
        start = (start[0], start[1] + step)
    return node_pos

def generate_bssid_list(data):
    bssid_list = []
    for timestamp, timestamp_dict in data.items():
        for bssid in timestamp_dict.keys():
            if bssid not in bssid_list:
                bssid_list.append(bssid)
    return bssid_list

def generate_ssid_list(data):
    ssid_list = []
    for timestamp, timestamp_dict in data.items():
        for bssid, bssid_dict in timestamp_dict.items():
            if bssid_dict["ssid"] not in ssid_list:
                ssid_list.append(bssid_dict["ssid"])
    return ssid_list

def write_json(filename, data):
    with open(filename, mode='w') as f:
        json.dump(data, f, indent=2)


def main():
    file_path = "measurement.csv"
    data = load_data(file_path)
    write_json("measurement.json", data)
    timestamp_list = list(data.keys())
    print("Number of Timestamps found: {}".format(len(timestamp_list)))
    timestamp_pos = generate_position(timestamp_list, (1,1), 100)


    bssid_list = generate_bssid_list(data)
    print("Number of BSSIDs found: {}".format(len(bssid_list)))
    bssid_pos = generate_position(bssid_list, (1000,1), 100)

    ssid_list = generate_ssid_list(data)
    print("Number of SSIDs found: {0}".format(len(ssid_list)))
    ssid_pos = generate_position(ssid_list, (2000, 1), 300)

    A = nx.Graph()
    # Add Timestamp Nodes
    for node in timestamp_list:
        A.add_node(node, pos=timestamp_pos[node])
    # Add BSSID Nodes
    for node in bssid_list:
        A.add_node(node, pos=bssid_pos[node])
    # Add SSID Nodes
    for node in ssid_list:
        A.add_node(node, pos=ssid_pos[node])

    # Add edges
    edges_color = []
    distances = {}
    for timestamp, timestamp_dict in data.items():
        ssids = {}
        for bssid in timestamp_dict.keys():
            # Get information about SSID per Timestamp
            if timestamp_dict[bssid]["ssid"] not in ssids.keys():
                ssids[timestamp_dict[bssid]["ssid"]] = {"number": 1, "bssids": [bssid]}
            else:
                ssids[timestamp_dict[bssid]["ssid"]]["number"] += 1
                ssids[timestamp_dict[bssid]["ssid"]]["bssids"].append(bssid)
            # Distances array for length histogram
            if timestamp_dict[bssid]["rssi"] not in distances.keys():
                distances[timestamp_dict[bssid]["rssi"]] = 1
            else:
                distances[timestamp_dict[bssid]["rssi"]] += 1

        for ssid, aps in ssids.items():
            if aps["number"] > 1:
                print("Discovered multiple ({2}) BSSIDs for SSID {0} within timestamp {1} - {3}".format(ssid, timestamp, aps["number"], aps["bssids"]))
            # Find the best BSSID for SSID
            best_bssid = None
            best_rssi = -1000
            for bssid in aps["bssids"]:
                if timestamp_dict[bssid]["rssi"] > best_rssi:
                    best_rssi = timestamp_dict[bssid]["rssi"]
                    best_bssid = bssid
            ssids[ssid]["best"] = best_bssid
            print("Best BSSID for timestamp {0} and SSID {1} is {2}".format(timestamp, ssid, best_bssid))

        for bssid in timestamp_dict.keys():
            is_best_bssid = False
            for ssid, aps in ssids.items():
                if bssid == aps["best"]:
                    is_best_bssid = True
            # Add edge between timestamp and BSSID
            A.add_edge(timestamp, bssid, length=timestamp_dict[bssid]["rssi"])
            if is_best_bssid:
                edges_color.append("green")
            else:
                edges_color.append("black")

            # Add edge between BSSID and SSID
            if not A.has_edge(bssid, timestamp_dict[bssid]["ssid"]):
                A.add_edge(bssid, timestamp_dict[bssid]["ssid"], length=1)
                if is_best_bssid:
                    edges_color.append("green")
                else:
                    edges_color.append("black")
            #A.add_path([timestamp, bssid, timestamp_dict[bssid]["ssid"]])

        #print(ssids)

    #bottom_nodes, top_nodes = bipartite.sets(A)

    color_map = []
    for node in A:
        # Timestamp
        if node in timestamp_list:
            color_map.append("blue")
        # BSSID
        elif node in bssid_list:
            color_map.append("red")
        # SSID
        else:
            color_map.append("green")
    # Get positions from graph
    pos = nx.get_node_attributes(A,'pos')

    plt.figure()
    # Bipartite - uncoment for bipartite graph

    nx.draw(A, pos=pos, node_color=color_map, edge_color=edges_color, with_labels=False)

    # Non-bipartite
    #nx.draw(A, node_color=color_map, edge_color=edges_color, with_labels=True)

    plt.draw()
    plt.show()
    #plt.savefig("A.png")

    # Histogram
    plt.figure()
    distances_x = []
    distances_y = []
    for k, v in distances.items():
        distances_x.append(k)
        distances_y.append(v)
    plt.bar(distances_x, distances_y)
    plt.draw()
    plt.show()



if __name__ == '__main__':
    main()
