#!/usr/bin/python
from __future__ import print_function
import networkx as nx
import csv
import os
import matplotlib.pyplot as plt


dir_path = os.path.dirname(os.path.realpath(__file__))

measurements_file = "measurement.csv"
measurements_file_path = "%s\%s" % (dir_path, measurements_file)
print(measurements_file_path)

data = []
lines = None
with open(measurements_file_path, mode="r") as file:
    lines = file.read().split("\r")

headers = lines[0]
lines = lines[1:-1]

for line in lines:
    splitline = line.split(";")
    data.append([splitline[2], splitline[7]])


print(data)
g = nx.Graph()
for line in data:
    g.add_edge(line[0], line[1])

nx.draw(g)
plt.draw()
plt.savefig("graph.png")
floyd = nx.floyd_warshall(g)
print(type(floyd))