import csv
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import random as rnd
from io import TextIOWrapper as TextCodecWrapper
import glob
import gzip

#=== close all
plt.clf()
fig = plt.gcf()
plt.close(fig)
plt.close('all')

#=== definice
G=nx.DiGraph()                                          # Graf stavů automatu
fsm = {}                                                # Hlavní slovník s daty o automatu
enterState='s42'
enterExit='s42'
defaultOutput='o98'

#=== načtení souboru CSV
with open('g1A03A.csv', "r") as csvFile:
    dataReader = csv.reader(csvFile, delimiter=',', quotechar='|')
    firstLines = 5
    for row in dataReader:
        if firstLines>=0:
            firstLines = firstLines - 1
            continue
                                                        #  Vytvoření slovníku o všech přechodech mezi stavy, jejich atributy a výstupy
                                                        # === IMPORT ZE SOUBORU
        fsm[row[0].strip()]={
          'e46': {'q': row[1].strip(),'o': row[5].strip(),'P':'1'},
          'e48': {'q': row[2].strip(),'o': row[6].strip(),'P':'1'},
          'e81': {'q': row[3].strip(),'o': row[7].strip(),'P':'1'}
           }
                                                        # Přidání uzlů a hran s atributy do Graphu
        G.add_edge(row[0].strip(), row[1].strip(), e='e46', o=row[5].strip())
        G.add_edge(row[0].strip(), row[2].strip(), e='e48', o=row[5].strip())
        G.add_edge(row[0].strip(), row[3].strip(), e='e81', o=row[5].strip())


L = {}                                                  # Pokryt stavu = mnozina vstupnich sekvenci L takova, ze lze
                                                        # nalezt prvek mnoziny L, kterym se lze dostat do jakehokoliv 
                                                        # zadaneho stavu z pocatecniho stavu q0.
T = {}
queue = set(fsm.keys()) - set(L.keys())                 # Slovník všech stavů automatu
shortestPath = nx.shortest_path(G, source='s42')        # vypocet nejkratší cesty od počátku s42
shortestPathLen = nx.shortest_path_length(G, source='s42') # vypocet delky nejkratši cesty od počátku s42

for a in sorted(queue):                                         # cyklus přes všechny stavy
    #SEKCE L
    L[a]=[]                                             # vytvořeni prázdného listu pro danný stav
    shortestPathA = shortestPath[a]                     # nejktraší cesta ke  dannému stavu a
    i = shortestPathLen[a]                              # delka shortestPathA
    while i > 0:                                        # cyklus podle počtu skoků v  nejktraší ceste (délka 3 = 3 cykly)
        #print(G[shortestPathA[i2]][shortestPathA[i]]['e'])
        L[a].append(G[shortestPathA[i - 1]][shortestPathA[i]]['e']) # přidání názvu hrany na nejkratší cestě do listu L[a]
                                                        #  ... opakuje se na každé hraně cesty!
        print(L[a])
        i = i-1                                         # Snížení ještě nezpracované delky cesty ve while
    L[a].reverse()                                      # otoceni poradi prvku ve vektoru
    # SEKCE T
    T[a] = []
    T[a].append(fsm[a]['e46']['q'])
    T[a].append(fsm[a]['e48']['q'])
    T[a].append(fsm[a]['e81']['q'])

print('Pokrytí stavů: ', list(L.values()))               # VÝPIS L
print('Pokrytí dosažitelných přechodů: ', T)             # VÝPIS T
#print(fsm['s52']['e48'])
fig = nx.draw_networkx(G)
#nx.write_gml(G,'G_statuses.gml')
#plt.show(fig)
print(fsm)

fsmC = fsm
sortQueue = sorted(queue)
for a in sortQueue:
    #del queue[a]
    queue.remove(a)
    for i in sorted(queue): #cmp()
        if fsmC[a]['e46'] == fsmC[i]['e46']:
            same1 = 1
        if fsmC[a]['e48'] == fsmC[i]['e48']:
            same2 = 1
        if fsmC[a]['e81'] == fsmC[i]['e81']:
            same3 = 1
