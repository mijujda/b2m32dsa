import csv

import os
import re
import json
import networkx as nx
import matplotlib.pyplot as plt
import itertools

class FSM:
    def __init__(self):
        self.states = set()
        self.transitions = {}
        self.events = set()
        self.outputs = set()
        self.entry_state = None
        self.exit_state = None
        self.default_output = None
        self.load_transitions("g1A05A.csv")

    def load_transitions(self, entry_file):
        cwd = os.path.dirname(__file__)
        print(cwd)
        pattern = re.compile(r"(?P<cur_state>s\d{2})\s+,\s(?P<state1>s\d{2}),\s(?P<state2>s\d{2}),\s(?P<state3>s\d{2}),\s+,(?P<output1>o\d{2}),\s(?P<output2>o\d{2}),\s(?P<output3>o\d{2})$")
        with open(f"{cwd}/{entry_file}", mode='r') as f:
            for line in f.readlines():
                #print(line)
                if "entry state" in line:
                    self.entry_state = line.split(" ")[-1].strip("\n")
                    print(f"Entry state: {self.entry_state}")
                    continue
                if "exit state" in line:
                    self.exit_state = line.split(" ")[-1].strip("\n")
                    print(f"Exit state: {self.exit_state}")
                    continue
                if "default output" in line:
                    self.default_output = line.split(" ")[-1].strip("\n")
                    print(f"Default output: {self.default_output}")
                    continue
                match = re.search(pattern,line)
                if match:
                    self.transitions[match.group("cur_state")] = {
                        "e01": [match.group("state1"), match.group("output1")],
                        "e66": [match.group("state2"), match.group("output2")],
                        "e82": [match.group("state3"), match.group("output3")]}
        for cur_state,transition in self.transitions.items():
            self.states.add(cur_state)
            for event in transition.keys():
                self.events.add(event)
                self.outputs.add(transition[event][1])


    def find_w_for_pair(self, q1, q2, max_steps=10):
        paths = None
        steps = 1
        q1_output, q2_output = None, None
        while steps <= max_steps:
            paths = itertools.permutations(self.events, steps)
            for path in paths:
                q1_output, q2_output = self.walk(q1, path)["outputs"], self.walk(q2, path)["outputs"]
                print(f"Path: {path}\tQ1: {q1_output}\tQ2: {q2_output}")
                if q1_output != q2_output:
                    print(f"Found! {path}")
                    return path
            steps += 1
        print(f"Error: Could not find W for given pair.")
        return None

    def find_w_for_all(self):
        state_pairs = itertools.combinations(self.states, 2)
        all_ws = set()
        for pair in state_pairs:
            all_ws.add(self.find_w_for_pair(pair[0], pair[1]))
        print(all_ws)


    def walk(self, start, path):
        outputs = []
        states = []
        cur_state = start
        for event in path:
            try:
                outputs.append(self.transitions[cur_state][event][1])
                states.append(self.transitions[cur_state][event][0])
                cur_state = states[-1]
            except KeyError:
                print(f"Error: No such event ({event}) for current state ({cur_state})")
                return None
        return {"states": [states], "outputs": outputs}

    def draw_fsm(self):
        G = nx.DiGraph()
        for start, events in self.transitions.items():
            for event in events.keys():
                for end in events[event][:1]:
                    G.add_edge(start, end)
        # Check if all nodes are reachable from start node
        if len(nx.shortest_path(G, source=self.entry_state)) < len(G.nodes()):
            print("FSM Has inaccessible state(s)")
        else:
            print(f"All FSM States are accessible from entry node {self.entry_state}")
        # Check if all nodes are reachable from exit node
        H = G.reverse()
        if len(nx.shortest_path(H, source=self.exit_state)) < len(H.nodes()):
            print("FSM Has dead-end state(s)")
        else:
            print(f"All FSM States have path(s) to reach exit node {self.exit_state}")
        plt.figure()

        pos = nx.circular_layout(G)
        nx.draw_networkx(G, pos=pos, with_labels=True)
        plt.draw()
        plt.show()





if __name__ == '__main__':
    fsm = FSM()
    fsm.draw_fsm()
    print(fsm.transitions)
    print(json.dumps(fsm.transitions, indent=2))
    for node, events in fsm.transitions.items():
        print(len(events))
    #fsm.find_w_for_all()
