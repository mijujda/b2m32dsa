import json
import matplotlib.pyplot as plt
import networkx as nx
import itertools

class FsmChecking:
    def __init__(self, transitions, start_node=None, exit_node=None, default_output=None):
        self.transitions = None
        self.start_node = start_node
        self.exit_node = exit_node
        self.default_output = default_output
        self.states = []
        self.events = []
        self.outputs = []
        self.graph = None

        self.load(transitions)
        self.get_graph()

    def load(self, transitions):
        new_transitions = {}
        # Adjust for transitions w/o outputs
        for current_state in transitions.keys():
            new_transitions[current_state] = {}
            for event in transitions[current_state].keys():
                if isinstance(transitions[current_state][event], list):
                    new_transitions[current_state][event] = {
                        "next_state": transitions[current_state][event][0],
                        "output": transitions[current_state][event][1]
                    }
                elif isinstance(transitions[current_state][event], str):
                    new_transitions[current_state][event] = {
                        "next_state": transitions[current_state][event],
                        "output": None
                    }
        # Find all possible states, events and outputs
        for current_state in new_transitions.keys():
            if current_state not in self.states:
                self.states.append(current_state)
            for event in new_transitions[current_state].keys():
                if event not in self.events:
                    self.events.append(event)
                if new_transitions[current_state][event]["output"] not in self.outputs:
                    self.outputs.append(new_transitions[current_state][event]["output"])
                if new_transitions[current_state][event]["next_state"] not in self.states:
                    self.states.append(new_transitions[current_state][event]["next_state"])
        self.transitions = new_transitions
        # Add default outputs, if given
        if self.default_output:
            for current_state in new_transitions:
                for event in self.events:
                    if event not in new_transitions[current_state].keys():
                        self.transitions[current_state][event] = {
                            "next_state": current_state,
                            "output": self.default_output
                        }

    def get_graph(self):
        self.graph = nx.MultiDiGraph()
        for current_state in self.transitions.keys():
            for event in self.transitions[current_state].keys():
                self.graph.add_edge(current_state, self.transitions[current_state][event]["next_state"])

        # Check if all nodes are reachable from start node
        if len(nx.shortest_path(self.graph, source=self.start_node)) < len(self.graph.nodes()):
            print("FSM Has inaccessible state(s)")
        else:
            print(f"All FSM States are accessible from entry node {self.start_node}")
        # Check if all nodes are reachable from exit node
        H = self.graph.reverse()
        if len(nx.shortest_path(H, source=self.exit_node)) < len(H.nodes()):
            print("FSM Has dead-end state(s)")
        else:
            print(f"All FSM States have path(s) to reach exit node {self.exit_node}")

        plt.figure()
        nx.draw(self.graph, with_labels=True)
        plt.draw()
        plt.show()

    def walk(self, start, path):
        """
        Based on start node and sequence of inputs (path),
        gives back list of outputs and states.
        """
        outputs = []
        states = []
        cur_state = start
        for event in path:
            try:
                outputs.append(self.transitions[cur_state][event]["output"])
                states.append(self.transitions[cur_state][event]["next_state"])
                cur_state = states[-1]
            except KeyError:
                print(f"Error: No such event ({event}) for current state ({cur_state})")
                return None
        return {"states": [states], "outputs": outputs}

    def reverse_walk(self, nodes):
        """
        Finds sequence of inputs to walk trough given nodes.
        """
        path = []
        for i in range(len(nodes)-1):
            for event in self.transitions[nodes[i]].keys():
                if self.transitions[nodes[i]][event]["next_state"] == nodes[i+1]:
                    path.append(event)
        print(path)
        return path


    def find_w_for_pair(self, q1, q2, max_steps=10):
        """
        Finds sequence of inputs to distinguish two states based on different output.
        """
        paths = None
        steps = 1
        q1_output, q2_output = None, None
        while steps <= max_steps:
            paths = [list(x) for x in itertools.permutations(self.events, steps)]
            for path in paths:
                q1_output, q2_output = self.walk(q1, path)["outputs"], self.walk(q2, path)["outputs"]
                #print(f"Path: {path}\tQ1: {q1_output}\tQ2: {q2_output}")
                if q1_output != q2_output:
                    #print(f"Found! {path}")
                    return path
            steps += 1
        print(f"Error: Could not find W for given pair.")
        return None

    def find_w_for_all(self):
        state_pairs = itertools.combinations(self.states, 2)
        all_ws = []
        for pair in state_pairs:
            w = self.find_w_for_pair(*pair)
            if w not in all_ws:
                all_ws.append(w)
        print(f"W: {all_ws}")

    def state_coverage(self):
        L = {}
        shortest_paths = nx.shortest_path(self.graph, source=self.start_node)
        print(shortest_paths)
        for target_state, nodes in shortest_paths.items():
            path = self.reverse_walk(nodes)
            print(f"Target state: {target_state} - Path: {path}")
            L[target_state] = path
        return L

def main():
    transitions = {
        "s96": {
            "e75": "s10",
            "e84": "s35"
        },
        "s35": {
            "e75": "s10"
        },
        "s10": {
            "e75": "s10",
            "e84": "s96"
        },
        "s75": {
            "e84": "s10",
            "e75": "s96"
        }
    }
    #transitions = json.loads('{"s01": {"e01": ["s53", "o21"], "e66": ["s41", "o74"], "e82": ["s39", "o74"]}, "s13": {"e01": ["s74", "o21"], "e66": ["s80", "o21"], "e82": ["s13", "o74"]}, "s18": {"e01": ["s74", "o21"], "e66": ["s53", "o21"], "e82": ["s32", "o21"]}, "s21": {"e01": ["s53", "o74"], "e66": ["s80", "o21"], "e82": ["s01", "o21"]}, "s31": {"e01": ["s54", "o21"], "e66": ["s74", "o74"], "e82": ["s13", "o21"]}, "s32": {"e01": ["s54", "o21"], "e66": ["s01", "o21"], "e82": ["s31", "o21"]}, "s39": {"e01": ["s53", "o74"], "e66": ["s31", "o74"], "e82": ["s39", "o21"]}, "s41": {"e01": ["s41", "o74"], "e66": ["s18", "o74"], "e82": ["s74", "o21"]}, "s42": {"e01": ["s21", "o21"], "e66": ["s21", "o21"], "e82": ["s18", "o74"]}, "s49": {"e01": ["s59", "o21"], "e66": ["s01", "o21"], "e82": ["s59", "o74"]}, "s53": {"e01": ["s54", "o74"], "e66": ["s80", "o21"], "e82": ["s59", "o21"]}, "s54": {"e01": ["s32", "o74"], "e66": ["s31", "o74"], "e82": ["s31", "o74"]}, "s59": {"e01": ["s13", "o74"], "e66": ["s49", "o74"], "e82": ["s39", "o74"]}, "s74": {"e01": ["s21", "o21"], "e66": ["s49", "o74"], "e82": ["s80", "o21"]}, "s80": {"e01": ["s13", "o21"], "e66": ["s39", "o74"], "e82": ["s42", "o21"]}}')

    fsm = FsmChecking(transitions=transitions, start_node="s75", exit_node="s10", default_output=None)
    #print(json.dumps(fsm.transitions, indent=2))
    print(fsm.state_coverage())

if __name__ == '__main__':
    main()
